import 'package:flutter/material.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(title: 'Steps Indicator Example'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedStep = 0;
  int nbSteps = 5;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            CircleAvatar(child: Image(image: ExactAssetImage("assets/pic.jpeg")),),
            Container(color: Colors.pink,width: 4,height: 30,),
            CircleAvatar(child: Image(image: ExactAssetImage("assets/pic.jpeg")),),
            Container(color: Colors.pink,width: 4,height: 30,),
            CircleAvatar(child: Image(image: ExactAssetImage("assets/pic.jpeg")),),
            Container(color: Colors.pink,width: 4,height: 30,),
            CircleAvatar(child: Image(image: ExactAssetImage("assets/pic.jpeg")),),
            Container(color: Colors.pink,width: 4,height: 30,),
            CircleAvatar(child: Image(image: ExactAssetImage("assets/pic.jpeg")),),
          ],
        ),
      ),
    );
  }
}





















//import 'package:flutter/material.dart';
//import 'package:steps/steps.dart';
////import 'package:audioplayers/audio_cache.dart';
//
//void main() => runApp(MaterialApp(
//      debugShowCheckedModeBanner: false,
//      home: Home(title: 'Steps'),
//    ));
//
//class Home extends StatelessWidget {
//  final String title;
//  Home({Key key, this.title}) : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text(title),
//        ),
//        body: Container(
//          alignment: Alignment.topCenter,
//          child: Steps(
//            direction: Axis.vertical,
//            size: 21.0,
//            path: {'color': Colors.lightBlue.shade200, 'width': 3.0},
//            steps: [
//              {
//                'color': Colors.white,
//                'background': Colors.lightBlue.shade200,
//                'label': '1',
//                'content': Text('step 01', style: TextStyle(fontSize: 22.0)),
//              },
//              {
//                'color': Colors.white,
//                'background': Colors.lightBlue.shade700,
//                'label': '2',
//                'content': Text('step 02', style: TextStyle(fontSize: 22.0))
//              },
//              {
//                'color': Colors.white,
//                'background': Colors.lightBlue.shade200,
//                'label': Icon(Icons.ac_unit).toString(),
//                'content': Text("step 03", style: TextStyle(fontSize: 22.0))
//              }
//            ],
//          ),
//        ));
//  }
//}

//
//
//class MyApp extends StatelessWidget {
//
//  Widget singlePiano(Color color,int soundNumber ){
//    return Expanded(
//      child: FlatButton(
//        color: color,
//        onPressed: (){
//          final player = AudioCache();
//          player.play('note$soundNumber.wav');
//        },
//      ),
//    );
//  }
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      debugShowCheckedModeBanner: false,
//      home: Scaffold(
//        backgroundColor: Colors.white,
//        body: SafeArea(
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.stretch,
//            children: <Widget>[
//              singlePiano(Colors.pink ,1),
//              singlePiano(Colors.teal ,2),
//              singlePiano(Colors.pink ,3),
//              singlePiano(Colors.teal ,4),
//              singlePiano(Colors.pink ,5),
//              singlePiano(Colors.teal ,6),
//              singlePiano(Colors.pink ,7),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
